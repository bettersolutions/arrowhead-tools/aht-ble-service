import os

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

if __name__ == "__main__":
    setuptools.setup(
        name="aht-ble-service",
        version=os.getenv("RELEASE_VERSION"),
        author="DAC SA",
        author_email="contact@dac.digital",
        description="Arrowhead BLE Service",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/bettersolutions/arrowhead-tools/aht-ble-service",
        entry_points={"console_scripts": ["gateway = gateway.__main__:run",
                                          "gateway-apply-migrations=db.migrations.apply:main"]},
        packages=setuptools.find_packages(),
        package_data={'db': ['alembic.ini']},
        include_package_data=True,
        setup_requires=['pytest-runner'],
        test_suite="tests",
        install_requires=[
            "starlette==0.19.0",
            "sse-starlette==0.10.3",
            "setuptools==62.0.0",
            "SQLAlchemy==1.4.32",
            "databases==0.5.5",
            "alembic==1.7.7",
            "SQLAlchemy-Utils==0.38.2",
            "uvicorn==0.17.6",
            "aiosqlite==0.17.0",
            "bleak==0.14.2",
        ],
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
        ],
        python_requires=">=3.6",
    )
