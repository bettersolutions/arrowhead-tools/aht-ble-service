# aht-ble-service

Arrowhead BLE Service

## Download from gitlab pypi registry

First, upgrade your pip:

```shell
python3 -m pip install -U pip
```

Then, you need to add gitlab index url
```shell
python3 -m pip install aht-ble-service --index-url=https://gitlab.com/api/v4/projects/29780875/packages/pypi/simple
```

## Init database:
Use command to init sqlite db in the current directory.
```shell
gateway-apply-migrations
```

## Run gateway:
Parameters host and port are optional. Below values are default
```shell
gateway --host="0.0.0.0" --port=8000
```
