import os

import pytest
from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database, drop_database
from starlette.config import environ
from starlette.testclient import TestClient

from db import db_dir

environ['TESTING'] = "True"


@pytest.fixture(autouse=True)
def create_test_database():
    from gateway import settings
    url = str(settings.DATABASE_URL)
    create_engine(url)
    assert not database_exists(url), "Test database already exists. Aborting tests."
    create_database(url)  # Create the test database.
    config = Config(os.path.join(db_dir, "alembic.ini"))
    config.set_main_option("script_location", os.path.join(db_dir, "migrations"))
    # Run the migrations.
    command.upgrade(config, "head")
    yield  # Run the tests.
    drop_database(url)


@pytest.fixture()
def client():
    from gateway import app

    with TestClient(app) as client:
        yield client
