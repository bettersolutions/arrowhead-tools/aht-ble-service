import asyncio
from uuid import uuid4

from gateway.gap import node_repository
from gateway.gatt import attribute_repository
from gateway.models import Attribute, Node


def test_insert_handle():
    # given
    attr_uuid = uuid4()
    attr = Attribute(attr_uuid)
    # when
    asyncio.run(attribute_repository.save(attr, 1))
    # then
    result = asyncio.run(attribute_repository.find_by_uuid_and_node_id(attr_uuid, 1))
    assert result.uuid == attr_uuid
    assert result.node_id == 1
    assert result.notification_enabled is False
    assert result.notification_support is False
    assert result.updated_at is not None
    assert result.value is None


def test_update_value():
    # given
    attr_uuid = uuid4()
    attr1 = Attribute(attr_uuid)
    attr2 = Attribute(attr_uuid)
    node1 = Node("address")
    node2 = Node("test")
    asyncio.run(node_repository.save(node1))
    asyncio.run(node_repository.save(node2))
    asyncio.run(attribute_repository.save(attr1, 1))
    asyncio.run(attribute_repository.save(attr2, 2))
    value = bytearray(b'\x08<\x00\x00')
    # when
    asyncio.run(attribute_repository.update_value_by_uuid_and_node_address(value, attr_uuid, "address"))
    # then
    result = asyncio.run(attribute_repository.find_by_uuid_and_node_id(attr_uuid, 1))
    assert result.uuid == attr_uuid
    assert result.node_id == 1
    assert result.value == value
    assert result.updated_at != attr1.updated_at
    attribute = asyncio.run(attribute_repository.find_by_uuid_and_node_id(attr_uuid, 2))
    assert attribute.uuid == attr_uuid
    assert attribute.node_id == 2
    assert attribute.value is None
    assert attribute.updated_at == attr2.updated_at


def test_update_notification_enabled():
    # given
    attr_uuid = uuid4()
    attr1 = Attribute(attr_uuid)
    attr2 = Attribute(attr_uuid)
    node1 = Node("address")
    node2 = Node("test")
    asyncio.run(node_repository.save(node1))
    asyncio.run(node_repository.save(node2))
    asyncio.run(attribute_repository.save(attr1, 1))
    asyncio.run(attribute_repository.save(attr2, 2))
    # when
    asyncio.run(attribute_repository.update_enabled_by_uuid_and_node_address(True, attr_uuid, "address"))
    # then
    result = asyncio.run(attribute_repository.find_by_uuid_and_node_id(attr_uuid, 1))
    assert result.uuid == attr_uuid
    assert result.node_id == 1
    assert result.notification_enabled is True
    assert result.updated_at == attr1.updated_at
    attribute = asyncio.run(attribute_repository.find_by_uuid_and_node_id(attr_uuid, 2))
    assert attribute.uuid == attr_uuid
    assert attribute.node_id == 2
    assert attribute.notification_enabled is False
    assert attribute.updated_at == attr2.updated_at
