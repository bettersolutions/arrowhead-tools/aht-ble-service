import json
from uuid import uuid4

import pytest
from pytest_mock import MockerFixture
from starlette.datastructures import URL
from starlette.testclient import TestClient


def test_get_nodes(client: TestClient, mocker: MockerFixture):
    # given
    mocker.patch("gateway.gap.node_controller.node_service.get_nodes", return_value={})
    # when
    result = client.get("/gap/nodes?active=1")
    # then
    assert result.status_code == 200
    assert result.content == b'{}'


def test_get_nodes_wrong_timeout_format(client: TestClient, mocker: MockerFixture):
    mock = mocker.patch("gateway.gap.node_controller.node_service.get_nodes", return_value={})
    # when
    result = client.get("/gap/nodes?timeout=notfloat")
    # then
    assert result.status_code == 400
    mock.assert_not_called()


def test_get_node_name(client: TestClient, mocker: MockerFixture):
    uuid = uuid4()
    response = {
        "self": {"href": f"http://testserver/gap/nodes/{uuid}"},
        "name": "Test"
    }
    mock = mocker.patch("gateway.gap.node_controller.node_service.get_node_name", return_value=response)
    # when
    result = client.get(f"/gap/nodes/{uuid}?name=1")
    # then
    assert result.status_code == 200
    assert json.loads(result.text) == response
    mock.assert_called_with(handle=uuid, hostname=URL("http://testserver/"))


right_params = [
    ("0", "0", "1"),
    ("1", "1", "30"),
    ("0", "1", "20.0"),
]


@pytest.mark.parametrize("connect,enable,interval", right_params)
def test_should_manage_connection_with_all_params(client: TestClient, mocker: MockerFixture, connect, enable, interval):
    # given
    mock = mocker.patch("gateway.gap.node_controller.node_service.manage_connection", return_value={})
    uuid = uuid4()
    # when
    result = client.put(f"/gap/nodes/{uuid}?connect={connect}&enable={enable}&interval={interval}")
    # then
    assert result.status_code == 200
    assert result.text == ""
    mock.assert_called_with(handle=uuid, connect=connect, enable=enable, interval=float(interval),
                            hostname=URL("http://testserver/"))


wrong_params = [
    (2, 1, 1, "Connect can be only 0 or 1"),
    (1, 2, 1, "Enable can be only 0 or 1"),
    (1, 1, "s", "Interval has to be a float"),
]


@pytest.mark.parametrize("connect,enable,interval,expected", wrong_params)
def test_should_raise_exception_when_wrong_params(client: TestClient, mocker: MockerFixture, connect, enable, interval,
                                                  expected):
    # given
    mock = mocker.patch("gateway.gap.node_controller.node_service.manage_connection")
    uuid = uuid4()
    # when
    result = client.put(f"/gap/nodes/{uuid}?connect={connect}&enable={enable}&interval={interval}")
    assert result.status_code == 400
    assert result.text == expected
    mock.assert_not_called()
