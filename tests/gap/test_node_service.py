import asyncio
from uuid import UUID, uuid4

from pytest_mock import MockerFixture
from starlette.datastructures import URL
from starlette.exceptions import HTTPException

from gateway.gap import node_service as test_obj
from gateway.gatt.connection import WorkerRequest
from gateway.models import Node

uuid = uuid4()
node = Node("address", handle=uuid, paired=False)
hostname: URL = URL("http://localhost:8000/")


def test_get_nodes(mocker: MockerFixture):
    address: str = "00:80:E1:26:DE:E0"
    advertisement = mocker.MagicMock()
    advertisement.address = address
    advertisement.name = '\x10\x18'
    find_node_mock = mocker.patch("gateway.gap.node_service.node_repository.find_by_address", return_value=node)
    save_node_mock = mocker.patch("gateway.gap.node_service.node_repository.save")
    mocker.patch("gateway.gap.node_service.bleak_utils.scan_for_nodes", return_value=[advertisement])
    # when
    result = asyncio.run(test_obj.get_nodes('active', 30, hostname))
    # then
    assert result["nodes"][0]["bdaddr"] == "00:80:E1:26:DE:E0"
    assert result["nodes"][0]["AD"] == [{'ADType': '0x09', 'ADValue': '1018'}]
    assert isinstance(UUID(result["nodes"][0]["handle"]), UUID)
    assert result["nodes"][0]["self"]["href"] == f"http://localhost:8000/gap/nodes/{result['nodes'][0]['handle']}"
    find_node_mock.assert_called_with("00:80:E1:26:DE:E0")
    save_node_mock.assert_not_called()


def test_should_unpair_node(mocker: MockerFixture):
    find_node_mock = mocker.patch("gateway.gap.node_service.node_repository.find_by_handle", return_value=node)
    update_node_mock = mocker.patch("gateway.gap.node_service.node_repository.update_paired")
    stop_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.stop_worker")
    start_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.start_worker")
    use_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.use_worker")
    # when
    asyncio.run(test_obj.manage_connection(uuid, None, "0", None, hostname))
    # then
    find_node_mock.assert_called_with(uuid)
    update_node_mock.assert_called_with("address", False)
    stop_worker_mock.assert_not_called()
    start_worker_mock.assert_not_called()
    use_worker_mock.assert_not_called()


def test_should_disconnect_node(mocker: MockerFixture):
    find_node_mock = mocker.patch("gateway.gap.node_service.node_repository.find_by_handle", return_value=node)
    update_node_mock = mocker.patch("gateway.gap.node_service.node_repository.update_paired")
    stop_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.stop_worker")
    start_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.start_worker")
    use_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.use_worker")
    # when
    asyncio.run(test_obj.manage_connection(uuid, "0", "1", None, hostname))
    # then
    find_node_mock.assert_called_with(uuid)
    stop_worker_mock.assert_called_with("address")
    update_node_mock.assert_called_with("address", False)
    start_worker_mock.assert_not_called()
    use_worker_mock.assert_not_called()


def test_should_connect_node(mocker: MockerFixture):
    find_node_mock = mocker.patch("gateway.gap.node_service.node_repository.find_by_handle", return_value=node)
    update_node_mock = mocker.patch("gateway.gap.node_service.node_repository.update_paired")
    stop_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.stop_worker")
    start_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.start_worker")
    use_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.use_worker")
    # when
    asyncio.run(test_obj.manage_connection(uuid, "1", "1", 30.0, hostname))
    # then
    find_node_mock.assert_called_with(uuid)
    start_worker_mock.assert_called_with("address")
    update_node_mock.assert_called_with("address", True)
    stop_worker_mock.assert_not_called()
    from gateway import bleak_utils
    use_worker_mock.assert_called_with("address", WorkerRequest(method=bleak_utils.pair_node, kwargs=dict()))


def test_should_raise_error_when_node_not_found(mocker: MockerFixture):
    find_node_mock = mocker.patch("gateway.gap.node_service.node_repository.find_by_handle", return_value=None)
    update_node_mock = mocker.patch("gateway.gap.node_service.node_repository.update_paired")
    stop_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.stop_worker")
    start_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.start_worker")
    use_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.use_worker")
    # when
    try:
        asyncio.run(test_obj.manage_connection(uuid, "1", "1", 30.0, hostname))
    except HTTPException as result:
        assert result.status_code == 404
        assert result.detail == f"Node with handle: {uuid} not found"
    # then
    find_node_mock.assert_called_with(uuid)
    start_worker_mock.assert_not_called()
    update_node_mock.assert_not_called()
    stop_worker_mock.assert_not_called()
    use_worker_mock.assert_not_called()


def test_should_raise_error_when_wrong_params(mocker: MockerFixture):
    find_node_mock = mocker.patch("gateway.gap.node_service.node_repository.find_by_handle", return_value=node)
    update_node_mock = mocker.patch("gateway.gap.node_service.node_repository.update_paired")
    stop_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.stop_worker")
    start_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.start_worker")
    use_worker_mock = mocker.patch("gateway.gap.node_service.conn_factory.use_worker")
    # when
    try:
        asyncio.run(test_obj.manage_connection(uuid, None, "1", 30.0, hostname))
    except HTTPException as result:
        assert result.status_code == 400
        assert result.detail == "Connect param missing"
    # then
    find_node_mock.assert_called_with(uuid)
    start_worker_mock.assert_not_called()
    update_node_mock.assert_not_called()
    stop_worker_mock.assert_not_called()
    use_worker_mock.assert_not_called()
