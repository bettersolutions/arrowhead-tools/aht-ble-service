import asyncio
from uuid import UUID

from gateway.gap import node_repository
from gateway.models import Node


def test_insert_node():
    # given
    node = Node("address")
    # when
    asyncio.run(node_repository.save(node))
    # then
    result = asyncio.run(node_repository.find_by_address("address"))
    assert result.bdaddr == "address"
    assert result.id == 1
    assert isinstance(result.handle, UUID)


def test_find_node_by_address():
    # given
    node = Node("address")
    asyncio.run(node_repository.save(node))
    # when
    result = asyncio.run(node_repository.find_by_address("address"))
    # then
    assert result.bdaddr == "address"
    assert result.id == 1
    assert isinstance(result.handle, UUID)


def test_find_node_by_handle():
    # given
    uuid = UUID('346ba86b-1e7d-41b1-b8b3-7b968fc39ed4')
    node = Node("address", handle=uuid)
    asyncio.run(node_repository.save(node))
    # when
    result = asyncio.run(node_repository.find_by_handle(uuid))
    # then
    assert result.handle == uuid
    assert result.bdaddr == "address"


def test_should_return_none_when_node_does_not_exists():
    # when
    result = asyncio.run(node_repository.find_by_address("address"))
    # then
    assert result is None


def test_should_update_node_paired():
    # given
    node = Node("address", paired=False)
    asyncio.run(node_repository.save(node))
    # when
    asyncio.run(node_repository.update_paired("address", True))
    # then
    result = asyncio.run(node_repository.find_by_address("address"))
    assert result.bdaddr == "address"
    assert result.id == 1
    assert isinstance(result.handle, UUID)
    assert result.paired is True


def test_find_all_paired_nodes():
    # given
    uuid = UUID('346ba86b-1e7d-41b1-b8b3-7b968fc39ed4')
    node = Node("address", handle=uuid, paired=True)
    asyncio.run(node_repository.save(node))
    asyncio.run(node_repository.save(Node("other_address", paired=False)))
    # when
    result = asyncio.run(node_repository.find_all_paired())
    # then
    assert len(result) == 1
    assert result[0].handle == uuid
    assert result[0].bdaddr == "address"
