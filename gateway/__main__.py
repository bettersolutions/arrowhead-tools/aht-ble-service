import argparse

import uvicorn

from gateway import app


def parse_cli_arguments():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--host", help="Declare host", default="0.0.0.0")
    argument_parser.add_argument("--port", help="Declare port", default=8000, type=int)
    return argument_parser.parse_args()


def run():
    args = parse_cli_arguments()
    uvicorn.run(app, host=args.host, port=args.port)


if __name__ == "__main__":
    run()
