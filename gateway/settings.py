import os

TESTING = os.environ.get("TESTING", False)
DATABASE_URL = os.environ.get("DATABASE_URL", "sqlite:///gateway.db")
if TESTING:
    DATABASE_URL = "sqlite:///test_gateway.db"
