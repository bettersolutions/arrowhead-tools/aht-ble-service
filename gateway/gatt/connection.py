import asyncio
import logging
from typing import Dict, Any, Mapping, Optional
from dataclasses import dataclass

from bleak import BleakClient, exc
from starlette.exceptions import HTTPException
from cysystemd.journal import JournaldLogHandler
from gateway.gap import node_service
from gateway.gatt import attribute_repository

logger = logging.getLogger(__name__)
logger.addHandler(JournaldLogHandler())


@dataclass
class WorkerRequest:
    method: Any
    kwargs: Mapping[str, Any]


class ConnectionWorker:
    def __init__(self, address: str):
        self.__address = address
        self.__queue: asyncio.Queue = asyncio.Queue()
        self.__error_queue: asyncio.Queue = asyncio.Queue()
        self.__start_event: asyncio.Event = asyncio.Event()
        self.__exit_event: asyncio.Event = asyncio.Event()
        self.__return_data_queue: asyncio.Queue = asyncio.Queue()

    async def start(self):
        asyncio.create_task(self._run_ble_client())
        await self.__start_event.wait()
        if not self.__error_queue.empty():
            error = await self.__error_queue.get()
            logger.error(f"Could not start connection worker due to: {error}")
            return error
        logger.info("Worker started.")

    async def stop(self):
        await self.__queue.put(None)
        await self.__exit_event.wait()
        logger.info("Worker stopped.")

    async def do(self, request) -> Any:
        await self.__queue.put(request)
        return await self.__return_data_queue.get()

    async def _run_ble_client(self):
        try:
            async with BleakClient(self.__address, disconnected_callback=self._disconnected_callback_handler) as client:
                logger.info(f"Connected: {client.is_connected}")
                self.__start_event.set()
                while True:
                    request: Optional[WorkerRequest] = await self.__queue.get()
                    logger.info(f"Request: {repr(request)}")
                    if request is None:
                        break
                    else:
                        response: Any = await request.method(client, **request.kwargs)
                        await self.__return_data_queue.put(response)
        except exc.BleakError as e:
            await self.__error_queue.put(e)
            self.__start_event.set()
        except asyncio.exceptions.TimeoutError as e:
            await self.__error_queue.put(e)
            self.__start_event.set()
        await self.__queue.put(None)

    def _disconnected_callback_handler(self, client):
        logger.error("Client disconnected")
        self.__exit_event.set()


class ConnectionFactory:
    def __init__(self):
        self.workers: Dict[str, ConnectionWorker] = dict()

    def __get_worker(self, address) -> ConnectionWorker:
        try:
            return self.workers[address]
        except KeyError:
            raise HTTPException(404, f"Worker with address: {address} not found")

    async def start_worker(self, address: str):
        if self.workers.get(address):
            raise HTTPException(404, f"Worker with address: {address} already exists")
        else:
            worker = ConnectionWorker(address)
            self.workers[address] = worker
            err = await worker.start()
            if err:
                del self.workers[address]
                raise err

    async def use_worker(self, address: str, request: WorkerRequest) -> Any:
        worker = self.__get_worker(address)
        return await worker.do(request)

    async def stop_worker(self, address: str):
        worker = self.__get_worker(address)
        await worker.stop()
        del self.workers[address]
        try:
            node_service.factory.workers[address].clear()
            await attribute_repository.update_enabled_by_node_address(False, address)
        except:
            pass
