from typing import Any, Dict, Optional, Union
from uuid import UUID

from sse_starlette import EventSourceResponse
from starlette.exceptions import HTTPException
from starlette.requests import HTTPConnection
from starlette.responses import JSONResponse
from starlette.routing import Route

from gateway.gatt import gatt_service
from gateway.gatt.gatt_service import NotificationRequest


async def get_services(request: HTTPConnection) -> JSONResponse:
    handle: UUID = request.path_params.get("handle")
    service_uuid: Optional[str] = request.query_params.get("uuid")
    if service_uuid:
        try:
            service_uuid: UUID = UUID(service_uuid)
        except ValueError:
            raise HTTPException(400, "Wrong UUID format")
    services: Dict[str, Any] = await gatt_service.get_services(handle=handle, service_uuid=service_uuid,
                                                               hostname=request.base_url)
    return JSONResponse(services)


async def get_characteristics(request: HTTPConnection) -> JSONResponse:
    node_handle: UUID = request.path_params.get("node_handle")
    service_handle: UUID = request.path_params.get("service_handle")
    characteristics: Dict[str, Any] = await gatt_service.get_characteristics(node_handle, service_handle,
                                                                             request.base_url)
    return JSONResponse(characteristics)


async def get_characteristic_by_uuid(request: HTTPConnection) -> JSONResponse:
    node_handle: UUID = request.path_params.get("handle")
    characteristic_uuid: Optional[str] = request.query_params.get("uuid")
    if characteristic_uuid is None:
        raise HTTPException(404, "UUID not found")
    try:
        characteristic_uuid: UUID = UUID(characteristic_uuid)
    except ValueError:
        raise HTTPException(400, "Wrong UUID format")
    characteristics: Dict[str, Any] = await gatt_service.get_characteristic_by_uuid(node_handle=node_handle,
                                                                                    characteristic_uuid=characteristic_uuid,
                                                                                    hostname=request.base_url)
    return JSONResponse(characteristics)


async def write_to_characteristic(request: HTTPConnection) -> JSONResponse:
    node_handle: UUID = request.path_params.get("node_handle")
    char_handle: UUID = request.path_params.get("char_handle")
    try:
        value: bytearray = bytearray(bytes.fromhex(request.path_params.get("value")))
    except ValueError:
        raise HTTPException(400, "Wrong value format")
    await gatt_service.write_to_characteristic(node_handle, char_handle, value)
    return JSONResponse("OK")


async def read_characteristic(request: HTTPConnection) -> JSONResponse:
    node_handle: UUID = request.path_params.get("node_handle")
    char_handle: UUID = request.path_params.get("characteristic_handle")

    value: Dict[str, Any] = await gatt_service.read_characteristic(node_handle, char_handle, request.base_url)
    return JSONResponse(value)


async def manage_notification(request: HTTPConnection) -> JSONResponse:
    notification_request: NotificationRequest = await _prepare_notification_request(request)
    await gatt_service.manage_notification(notification_request)
    return JSONResponse("OK")


async def get_value(request: HTTPConnection) -> JSONResponse:
    notify: Optional[bool] = _get_optional_bool_query_param(request, "notify")
    indicate: Optional[bool] = _get_optional_bool_query_param(request, "indicate")
    if notify is None and indicate is None:
        return await read_characteristic(request)
    return await get_notification_value(request)


async def get_notification_value(request: HTTPConnection) -> Union[JSONResponse, EventSourceResponse]:
    notification_request: NotificationRequest = await _prepare_notification_request(request)
    if notification_request.event:
        generator = await gatt_service.stream_notifications(notification_request)
        return EventSourceResponse(generator)
    result: Dict[str, Any] = await gatt_service.get_notification_value(notification_request, request.base_url)
    return JSONResponse(result)


async def _prepare_notification_request(request) -> NotificationRequest:
    node_handle: UUID = request.path_params.get("node_handle")
    characteristic_handle: UUID = request.path_params.get("characteristic_handle")
    notify: Optional[bool] = _get_optional_bool_query_param(request, "notify")
    indicate: Optional[bool] = _get_optional_bool_query_param(request, "indicate")
    event: Optional[bool] = _get_optional_bool_query_param(request, "event")
    if notify is None and indicate is None:
        raise HTTPException(400, "Either notify or indicate parameter has to be declared")
    if notify is not None and indicate is not None:
        raise HTTPException(400, "Both notify and indicate parameters cannot be declared")
    return NotificationRequest(node_handle=node_handle, characteristic_handle=characteristic_handle, notify=notify,
                               indicate=indicate, event=event)


def _get_optional_bool_query_param(request: HTTPConnection, param_name: str) -> Optional[bool]:
    param: Optional[str] = request.query_params.get(param_name)
    if param:
        if param == "0":
            param: bool = False
        elif param == "1":
            param: bool = True
        else:
            raise HTTPException(400, f"{param_name} can be only 0 or 1")
    return param


routes = [
    Route("/nodes/{handle:uuid}/services", endpoint=get_services, methods=["GET", ]),
    Route("/nodes/{node_handle:uuid}/services/{service_handle:uuid}/characteristics", endpoint=get_characteristics,
          methods=["GET", ]),
    Route("/nodes/{handle:uuid}/characteristics", endpoint=get_characteristic_by_uuid, methods=["GET", ]),
    Route("/nodes/{node_handle:uuid}/characteristics/{char_handle:uuid}/value/{value}",
          endpoint=write_to_characteristic, methods=["PUT", ]),
    Route("/nodes/{node_handle:uuid}/characteristics/{characteristic_handle:uuid}", endpoint=manage_notification,
          methods=["PUT", ]),
    Route("/nodes/{node_handle:uuid}/characteristics/{characteristic_handle:uuid}/value", endpoint=get_value,
          methods=["GET", ])
]
