from datetime import datetime
from typing import Optional
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.sql import ClauseElement

from gateway.db import database
from gateway.models import Attribute, attribute_table, node_table


async def save(attribute: Attribute, node_id: int):
    query = attribute_table.insert().values(
        handle=attribute.handle,
        uuid=attribute.uuid,
        node_id=node_id,
        notification_enabled=attribute.notification_enabled,
        notification_support=attribute.notification_support,
        value=attribute.value,
        updated_at=attribute.updated_at,
    )
    await database.execute(query)


async def find_by_handle_and_node_id(handle: UUID, node_id) -> Optional[Attribute]:
    query = select(Attribute).where(Attribute.handle == handle).where(Attribute.node_id == node_id)
    return await database.fetch_one(query)


async def find_by_uuid_and_node_id(uuid: UUID, node_id: int) -> Optional[Attribute]:
    query = select(Attribute).where(Attribute.uuid == uuid).where(Attribute.node_id == node_id)
    return await database.fetch_one(query)


async def update_support_by_attribute_handle(support: bool, handle: UUID):
    query = attribute_table.update().where(Attribute.handle == handle).values(notification_support=support)
    await database.execute(query)


async def update_value_by_uuid_and_node_address(value: str, uuid: UUID, address: str):
    query = __update_query(address, uuid).values(value=value, updated_at=datetime.utcnow())
    await database.execute(query)


async def update_enabled_by_uuid_and_node_address(enabled: bool, uuid: UUID, address: str):
    query = __update_query(address, uuid).values(notification_enabled=enabled)
    await database.execute(query)


async def update_enabled_by_node_address(enabled: bool, address: str):
    query = attribute_table.update().where(
        address == select(
            [node_table.c.bdaddr]
        ).where(
            node_table.c.id == attribute_table.c.node_id
    ).as_scalar()
    ).values(notification_enabled=enabled)
    await database.execute(query)


def __update_query(address: str, uuid: UUID) -> ClauseElement:
    return attribute_table.update().where(
        Attribute.uuid == uuid
    ).where(
        address == select(
            [node_table.c.bdaddr]
        ).where(
            node_table.c.id == attribute_table.c.node_id
        ).as_scalar()
    )
