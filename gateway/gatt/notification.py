import asyncio
import binascii
import logging
from typing import Dict, Optional
from uuid import UUID

from bleak import BleakClient, exc
from starlette.exceptions import HTTPException
from cysystemd.journal import JournaldLogHandler

from gateway.gatt import attribute_repository

logger = logging.getLogger(__name__)
logger.addHandler(JournaldLogHandler())


class NotificationWorker:
    def __init__(self, address: str, char_uuid: UUID):
        self.notification_condition: asyncio.Condition = asyncio.Condition()
        self.__address = address
        self.__char_uuid = char_uuid
        self.__queue: asyncio.Queue = asyncio.Queue()
        self.__error_queue: asyncio.Queue = asyncio.Queue()
        self.__stop_event: asyncio.Event = asyncio.Event()
        self.__start_event: asyncio.Event = asyncio.Event()
        self.__exit_event: asyncio.Event = asyncio.Event()

    async def start(self, client: BleakClient):
        asyncio.create_task(self._run_ble_client(client))
        asyncio.create_task(self._run_queue_consumer())
        await self.__start_event.wait()
        if not self.__error_queue.empty():
            error = await self.__error_queue.get()
            logger.error(f"Could not start notification worker due to: {error}")
            raise error
        await attribute_repository.update_enabled_by_uuid_and_node_address(True, self.__char_uuid, self.__address)
        logger.info("Worker started.")

    async def stop(self):
        self.__stop_event.set()
        await self.__exit_event.wait()
        await attribute_repository.update_enabled_by_uuid_and_node_address(False, self.__char_uuid, self.__address)
        logger.info("Worker stopped.")

    async def _run_ble_client(self, client: BleakClient):
        try:
            await client.start_notify(self.__char_uuid, self._callback_handler)
            self.__start_event.set()
            await self.__stop_event.wait()
            await client.stop_notify(self.__char_uuid)
        except exc.BleakError as e:
            await self.__error_queue.put(e)
            self.__start_event.set()
        except asyncio.exceptions.TimeoutError as e:
            await self.__error_queue.put(e)
            self.__start_event.set()
        await self.__queue.put(None)

    def _callback_handler(self, sender, data):
        self.__queue.put_nowait(data)

    async def _run_queue_consumer(self):
        while True:
            data: Optional[bytearray] = await self.__queue.get()
            if data is None:
                logger.info("Got message from client about disconnection. Exiting consumer loop...")
                break
            else:
                logger.info(f"Received callback data via async queue: {data}")
                data: str = binascii.hexlify(data).decode()
                await attribute_repository.update_value_by_uuid_and_node_address(data, self.__char_uuid, self.__address)
                logger.info(f"Data saved")
                async with self.notification_condition:
                    self.notification_condition.notify_all()
        self.__exit_event.set()


class NotificationFactory:
    def __init__(self):
        self.workers: Dict[str, Dict[UUID, NotificationWorker]] = dict()

    async def start_worker(self, client: BleakClient, address: str, char_uuid: UUID):
        worker = NotificationWorker(address, char_uuid)
        worker_mapping = self.workers.get(address)
        if worker_mapping is None:
            self.workers[address] = {char_uuid: worker}
        else:
            self.workers[address][char_uuid] = worker
        await worker.start(client)

    async def stop_worker(self, client: BleakClient, address: str, char_uuid: UUID):
        worker = self.__get_worker(address, char_uuid)
        await worker.stop()
        del self.workers[address][char_uuid]

    def get_worker_notification_condition(self, address: str, char_uuid: UUID) -> asyncio.Condition:
        worker = self.__get_worker(address, char_uuid)
        return worker.notification_condition

    def __get_worker(self, address: str, char_uuid: UUID) -> NotificationWorker:
        try:
            return self.workers[address][char_uuid]
        except KeyError:
            raise HTTPException(404, f"Worker with address: {address} and characteristic: {char_uuid} not found")
