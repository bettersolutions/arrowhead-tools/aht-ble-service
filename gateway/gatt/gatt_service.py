import asyncio
import logging
from dataclasses import dataclass
from typing import List, Optional, Any, Dict, Iterable, Mapping
from uuid import UUID
import binascii

from bleak import BleakError
from bleak.backends.characteristic import BleakGATTCharacteristic
from bleak.backends.service import BleakGATTService
from starlette.datastructures import URL
from starlette.exceptions import HTTPException
from cysystemd.journal import JournaldLogHandler

from gateway import bleak_utils
from gateway.gap import node_repository
from gateway.gatt import attribute_repository
from gateway.models import Node, Attribute
from gateway.gap.node_service import conn_factory, factory
from gateway.gatt.connection import WorkerRequest

logger = logging.getLogger(__name__)
logger.addHandler(JournaldLogHandler())

properties_constants = {
    "broadcast": 0x0001,
    "read": 0x0002,
    "write_without_response": 0x0004,
    "write": 0x0008,
    "notify": 0x0010,
    "indicate": 0x0020,
    "authenticated_signed_writes": 0x0040,
    "extended_properties": 0x0080,
    "reliable_write": 0x0100,
    "writable_auxiliaries": 0x0200
}


@dataclass
class NotificationRequest:
    node_handle: UUID
    characteristic_handle: UUID
    notify: Optional[bool]
    indicate: Optional[bool]
    event: Optional[bool]


async def get_services(handle, service_uuid: Optional[str], hostname: URL):
    node = await _find_node_or_404(handle)
    address: str = node.bdaddr
    request: WorkerRequest = WorkerRequest(method=bleak_utils.get_services, kwargs=dict(service_uuid=service_uuid))
    services_list: Iterable[BleakGATTService] = await conn_factory.use_worker(address,
                                                                               request)
    services = []

    for service in services_list:
        attribute: Optional[Attribute] = await attribute_repository.find_by_uuid_and_node_id(UUID(service.uuid),
                                                                                             node.id)
        if not attribute:
            attribute = Attribute(UUID(service.uuid))
            await attribute_repository.save(attribute, node.id)

        service_dict = {
            "self": {"href": f"{hostname}gatt/nodes/services/{attribute.handle}"},
            "handle": str(attribute.handle),
            "uuid": service.uuid,
            "primary": "true"
        }
        services.append(service_dict)

    return {"services": services}


async def get_characteristics(node_handle, service_handle, hostname: URL):
    node = await _find_node_or_404(node_handle)
    service = await _find_attribute_or_404(service_handle, node.id)
    address: str = node.bdaddr
    request: WorkerRequest = WorkerRequest(method=bleak_utils.get_characteristics, kwargs=dict(service_uuid=service.uuid))
    characteristics_list: List[BleakGATTCharacteristic] = await conn_factory.use_worker(address, request)
    characteristics = []

    for characteristic in characteristics_list:
        attribute_entity: Attribute = await _prepare_entity(characteristic, node)
        characteristic_dict = await _get_characteristic_dict(characteristic, hostname, attribute_entity)
        characteristics.append(characteristic_dict)
    return {"characteristics": characteristics}


async def get_characteristic_by_uuid(node_handle, characteristic_uuid: UUID, hostname: URL):
    node = await _find_node_or_404(node_handle)
    address: str = node.bdaddr
    request: WorkerRequest = WorkerRequest(method=bleak_utils.get_characteristic_by_uuid, kwargs=dict(characteristic_uuid=characteristic_uuid))
    characteristic: BleakGATTCharacteristic = await conn_factory.use_worker(address, request)
    characteristics = []

    attribute_entity: Attribute = await _prepare_entity(characteristic, node)
    characteristic_dict = await _get_characteristic_dict(characteristic, hostname, attribute_entity)
    characteristics.append(characteristic_dict)
    return {"characteristics": characteristics}


async def write_to_characteristic(node_handle: UUID, char_handle: UUID, value: bytearray):
    node: Node = await _find_node_or_404(node_handle)
    characteristic: Attribute = await _find_attribute_or_404(char_handle, node.id)
    request: WorkerRequest = WorkerRequest(method=bleak_utils.write_to_characteristic, kwargs=dict(characteristic_uuid=characteristic.uuid, value=value))
    await conn_factory.use_worker(node.bdaddr, request)


async def read_characteristic(node_handle: UUID, char_handle: UUID, hostname: URL):
    node: Node = await _find_node_or_404(node_handle)
    characteristic: Attribute = await _find_attribute_or_404(char_handle, node.id)
    request: WorkerRequest = WorkerRequest(method=bleak_utils.read_characteristic, kwargs=dict(characteristic_uuid=characteristic.uuid))
    char_value: bytearray = await conn_factory.use_worker(node.bdaddr, request)
    return {
        "self": {"href": f"{hostname}gatt/nodes/{node_handle}/characteristics/{char_handle}"},
        "handle": str(char_handle),
        "value": str(binascii.hexlify(char_value).decode()),
    }


async def manage_notification(request: NotificationRequest):
    node = await _find_node_or_404(request.node_handle)
    characteristic = await _find_attribute_or_404(request.characteristic_handle, node.id)
    if request.notify is not None or request.indicate is not None:
        try:
            if request.notify or request.indicate:
                request: WorkerRequest = WorkerRequest(method=factory.start_worker,
                                                       kwargs=dict(address=node.bdaddr, char_uuid=characteristic.uuid))
                await conn_factory.use_worker(node.bdaddr, request)
            else:
                request: WorkerRequest = WorkerRequest(method=factory.stop_worker,
                                                       kwargs=dict(address=node.bdaddr, char_uuid=characteristic.uuid))
                await conn_factory.use_worker(node.bdaddr, request)
        except BleakError as e:
            raise HTTPException(status_code=404, detail=e.args[0])


async def get_notification_value(request: NotificationRequest, hostname: URL) -> Dict[str, Any]:
    node = await _find_node_or_404(request.node_handle)
    characteristic = await _find_attribute_or_404(request.characteristic_handle, node.id)
    if not characteristic.notification_support or not characteristic.notification_enabled:
        raise HTTPException(status_code=412, detail="Indication or notification not supported or not activated")
    if request.notify is not None or request.indicate is not None:
        if request.notify or request.indicate:
            return {
                "self": {
                    "href": f"{hostname}gatt/nodes/{request.node_handle}/characteristics/{request.characteristic_handle}"},
                "value": characteristic.value,
            }
    raise HTTPException(status_code=400, detail="Notify/indicate param have to be equal to 1")


async def stream_notifications(request: NotificationRequest) -> Mapping[str, str]:
    node = await _find_node_or_404(request.node_handle)
    characteristic = await _find_attribute_or_404(request.characteristic_handle, node.id)
    if not characteristic.notification_support or not characteristic.notification_enabled:
        raise HTTPException(status_code=412, detail="Indication or notification not supported or not activated")
    if request.notify is not None or request.indicate is not None:
        if request.notify or request.indicate:
            return __stream_notifications(node, characteristic)


async def __stream_notifications(node: Node, characteristic: Attribute) -> Mapping[str, str]:
    notification_ready = factory.get_worker_notification_condition(node.bdaddr, characteristic.uuid)
    try:
        while True:
            async with notification_ready:
                await notification_ready.wait()
                characteristic = await _find_attribute_or_404(characteristic.handle, node.id)
                yield dict(data=characteristic.value)
    except asyncio.CancelledError as e:
        logger.error(f"Disconnected from client (via refresh/close)")
        # Do any other cleanup, if any
        raise e


async def _prepare_entity(characteristic: BleakGATTCharacteristic, node: Node) -> Attribute:
    notification_support = False
    if "notify" in characteristic.properties or "indicate" in characteristic.properties:
        notification_support = True
    attribute_entity = await _get_attribute(characteristic.uuid, node.id, notification_support)
    if attribute_entity.notification_support is not notification_support:
        await attribute_repository.update_support_by_attribute_handle(notification_support, attribute_entity.handle)
    return attribute_entity


async def _get_attribute(characteristic_uuid: str, node_id: int, notification_support: bool) -> Attribute:
    attribute_entity = await attribute_repository.find_by_uuid_and_node_id(UUID(characteristic_uuid), node_id)
    if not attribute_entity:
        attribute_entity = Attribute(uuid=UUID(characteristic_uuid), notification_support=notification_support)
        await attribute_repository.save(attribute_entity, node_id)
    return attribute_entity


async def _get_characteristic_dict(characteristic: BleakGATTCharacteristic, hostname: URL, attribute: Attribute) -> \
        Dict[str, Any]:
    properties = 0x0
    for item in characteristic.properties:
        for key, value in properties_constants.items():
            if item == key:
                properties += value
    return {
        "self": {"href": f"{hostname}gatt/nodes/characteristics/{attribute.handle}"},
        "handle": str(attribute.handle),
        "uuid": characteristic.uuid,
        "properties": f"0x{properties:02x}"
    }


async def _find_node_or_404(node_handle) -> Node:
    node: Optional[Node] = await node_repository.find_by_handle(node_handle)
    if not node:
        raise HTTPException(404, f"Node with handle: {node_handle} not found")
    return node


async def _find_attribute_or_404(attribute_handle: UUID, node_id: int) -> Attribute:
    attribute: Optional[Attribute] = await attribute_repository.find_by_handle_and_node_id(attribute_handle, node_id)
    if not attribute:
        raise HTTPException(404, f"Attribute with handle: {attribute_handle} not found")
    return attribute
