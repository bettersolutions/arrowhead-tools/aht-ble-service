from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from typing import List
from uuid import UUID, uuid4

import sqlalchemy
from sqlalchemy.orm import registry, relationship
from sqlalchemy_utils import UUIDType

metadata = sqlalchemy.MetaData()
mapper_registry = registry()


@dataclass
class Attribute:
    id: int = field(init=False, default_factory=int)
    uuid: UUID
    handle: UUID = field(init=True, default_factory=lambda: uuid4())
    node_id: int = field(init=True, default_factory=int)
    notification_enabled: bool = field(init=True, default=False)
    notification_support: bool = field(init=True, default=False)
    value: str = field(init=True, default=None)
    updated_at: datetime = field(init=False, default_factory=lambda: datetime.utcnow())


@dataclass
class Node:
    id: int = field(init=False, default_factory=int)
    bdaddr: str
    handle: UUID = field(init=True, default_factory=lambda: uuid4())
    paired: bool = field(init=True, default=False)
    attributes: List[Attribute] = field(default_factory=list)


attribute_table = sqlalchemy.Table(
    "attributes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True, autoincrement=True),
    sqlalchemy.Column("uuid", UUIDType(binary=False)),
    sqlalchemy.Column("handle", UUIDType(binary=False), unique=True),
    sqlalchemy.Column('node_id', sqlalchemy.Integer, sqlalchemy.ForeignKey('nodes.id')),
    sqlalchemy.Column("notification_enabled", sqlalchemy.Boolean, default=False),
    sqlalchemy.Column("notification_support", sqlalchemy.Boolean, default=False),
    sqlalchemy.Column("value", sqlalchemy.String(length=255)),
    sqlalchemy.Column("updated_at", sqlalchemy.DateTime, default=datetime.utcnow),
)

node_table = sqlalchemy.Table(
    "nodes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True, autoincrement=True),
    sqlalchemy.Column("handle", UUIDType(binary=False), unique=True),
    sqlalchemy.Column("bdaddr", sqlalchemy.String(255), unique=True),
    sqlalchemy.Column("paired", sqlalchemy.Boolean, default=False)
)

mapper_registry.map_imperatively(Node, node_table, properties={
    'characteristics': relationship(Attribute, backref='node', order_by=attribute_table.c.id),
})
mapper_registry.map_imperatively(Attribute, attribute_table)
