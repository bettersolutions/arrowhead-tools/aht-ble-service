from bleak import exc
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Mount

import gateway.gap.node_controller
import gateway.gatt.gatt_controller
from gateway.db import database

routes = [
    Mount("/gap", routes=gateway.gap.node_controller.routes),
    Mount("/gatt", routes=gateway.gatt.gatt_controller.routes)
]
app = Starlette(
    routes=routes,
    on_startup=[database.connect],
    on_shutdown=[database.disconnect]
)


@app.exception_handler(exc.BleakError)
async def unicorn_exception_handler(request, e):
    return JSONResponse(
        status_code=504,
        content=e.args[0],
    )
