from typing import Optional, Iterable, List
from uuid import UUID

from bleak import BleakScanner, BleakClient
from bleak.backends.characteristic import BleakGATTCharacteristic
from bleak.backends.device import BLEDevice
from bleak.backends.service import BleakGATTServiceCollection, BleakGATTService
from starlette.exceptions import HTTPException


async def scan_for_nodes(timeout: float, mode: str = 'active') -> List[BLEDevice]:
    timeout = 30.0 if not timeout else timeout
    return await BleakScanner.discover(scanning_mode=mode, timeout=timeout)


async def get_node_name(address: str):
    device = await BleakScanner.find_device_by_address(address)
    return device.name if hasattr(device, 'name') else ""


async def pair_node(client: BleakClient):
    try:
        await client.pair()
    except:
        raise HTTPException(404, f"Pairing failed.")


async def get_services(client: BleakClient, service_uuid: Optional[UUID]) -> Iterable[BleakGATTService]:
    services: BleakGATTServiceCollection = await client.get_services()
    if service_uuid:
        service: BleakGATTService = services.get_service(service_uuid)
        if service:
            return [service]
        raise HTTPException(404, f"Service not found")
    return services


async def get_characteristics(client: BleakClient, service_uuid: UUID) -> List[BleakGATTCharacteristic]:
    services: BleakGATTServiceCollection = await client.get_services()
    service: BleakGATTService = services.get_service(service_uuid)
    if service:
        return service.characteristics
    raise HTTPException(404, f"Service not found")


async def get_characteristic_by_uuid(client: BleakClient, characteristic_uuid: UUID) -> BleakGATTCharacteristic:
    services: BleakGATTServiceCollection = await client.get_services()
    char: BleakGATTCharacteristic = services.get_characteristic(characteristic_uuid)
    if char:
        return char
    raise HTTPException(404, f"Characteristic not found")


async def write_to_characteristic(client: BleakClient, characteristic_uuid: UUID, value: bytearray):
    await client.write_gatt_char(str(characteristic_uuid), value)


async def read_characteristic(client: BleakClient, characteristic_uuid: UUID):
    return await client.read_gatt_char(str(characteristic_uuid))
