import binascii
import re
from typing import List, Optional, Mapping, Any
from uuid import UUID

from bleak.backends.device import BLEDevice
from starlette.datastructures import URL
from starlette.exceptions import HTTPException

from gateway import bleak_utils
from gateway.bleio_uuid import BLEIO_UUID
from gateway.gap import node_repository
from gateway.gatt.connection import ConnectionFactory, WorkerRequest
from gateway.gatt.notification import NotificationFactory
from gateway.models import Node

conn_factory = ConnectionFactory()
factory = NotificationFactory()


async def get_nodes(scanning_mode: str, timeout: Optional[float], hostname: URL):
    nodes_list: List[BLEDevice] = await bleak_utils.scan_for_nodes(timeout, scanning_mode)
    nodes = []

    for device in nodes_list:
        address = device.address
        node = await _get_node_by_address(address)
        data_dict = _data_dict_from_metadata(device)

        ad_list = []
        for adtype, value in data_dict.items():
            ad_list.append(
                {
                    "ADType": "0x{:02x}".format(adtype),
                    "ADValue": binascii.hexlify(value).decode()
                })

        node_dict = {
            "self": _get_gap_node_self_object(hostname, node.handle),
            "handle": str(node.handle),
            "bdaddr": address,
            "AD": ad_list
        }
        nodes.append(node_dict)
    return {"nodes": nodes}


async def get_paired_nodes(handle: Optional[UUID], hostname: URL) -> Mapping[str, Any]:
    if handle:
        device: Optional[Node] = await node_repository.find_by_handle(handle)
        nodes_list = [device] if device.paired else []
    else:
        nodes_list: List[Node] = await node_repository.find_all_paired()

    return _get_nodes_response(hostname, nodes_list)


async def get_node_name(handle: UUID, hostname: URL) -> Mapping[str, Any]:
    node: Optional[Node] = await node_repository.find_by_handle(handle)
    if not node:
        raise HTTPException(404, f"Node with handle: {handle} not found")
    address: str = node.bdaddr
    node_name = await bleak_utils.get_node_name(address)

    return {
        "self": _get_gap_node_self_object(hostname, node.handle),
        "name": node_name
    }


async def manage_connection(handle: UUID, connect: Optional[str], enable: Optional[str], interval: Optional[float],
                            hostname: URL) -> Optional[Mapping[str, Any]]:
    node: Optional[Node] = await node_repository.find_by_handle(handle)
    if not node:
        raise HTTPException(404, f"Node with handle: {handle} not found")
    address: str = node.bdaddr

    if enable == "0":
        await node_repository.update_paired(address, False)
        return

    if connect == "0":
        await conn_factory.stop_worker(address)
        await node_repository.update_paired(address, False)
        return
    elif connect == "1":
        await connect_node(address=address, enable=enable, interval=interval)
        return

    return _get_nodes_response(hostname, [node])


async def connect_node(address: str, enable: Optional[str] = None, interval: Optional[float] = None):
    await conn_factory.start_worker(address)
    if enable == "1":
        request: WorkerRequest = WorkerRequest(method=bleak_utils.pair_node, kwargs=dict())
        await conn_factory.use_worker(address, request)
        await node_repository.update_paired(address, True)


def _get_nodes_response(hostname: URL, nodes_list: List[Node]) -> Mapping[str, Any]:
    nodes = []
    for node in nodes_list:
        node_dict = {
            "self": _get_gap_node_self_object(hostname, node.handle),
            "handle": str(node.handle),
            "bdaddr": node.bdaddr,
            "AD": []
        }
        nodes.append(node_dict)
    return {"nodes": nodes}


async def _get_node_by_address(address):
    node: Optional[Node] = await node_repository.find_by_address(address)
    if not node:
        node = Node(address)
        await node_repository.save(node)
    return node


def _get_gap_node_self_object(hostname: URL, node_handle: UUID) -> Mapping[str, str]:
    return {"href": f"{hostname}gap/nodes/{node_handle}"}


def _data_dict_from_metadata(device):
    _RE_IGNORABLE_NAME = re.compile(
        r"((dev_)?"
        r"[0-9A-F]{2}[-_][0-9A-F]{2}[-_][0-9A-F]{2}[-_][0-9A-F]{2}[-_][0-9A-F]{2}[-_][0-9A-F]{2})"
        r"|Unknown",
        re.IGNORECASE,
    )
    data_dict = {}
    for key, value in device.metadata.items():
        if key == "manufacturer_data":
            # The manufacturer data value is a dictionary.
            # Re-concatenate it into bytes
            all_mfr_data = bytearray()
            for mfr_id, mfr_data in value.items():
                all_mfr_data.extend(mfr_id.to_bytes(2, byteorder="little"))
                all_mfr_data.extend(mfr_data)
            data_dict[0xFF] = all_mfr_data
        elif key == "uuids":
            uuids16 = bytearray()
            uuids128 = bytearray()
            for uuid in value:
                bleio_uuid = BLEIO_UUID(uuid)
                # If this is a Standard UUID in 128-bit form, convert it to a 16-bit UUID.
                if bleio_uuid.is_standard_uuid:
                    uuids16.extend(bleio_uuid.uuid128[12:14])
                else:
                    uuids128.extend(bleio_uuid.uuid128)

        if uuids16:
            # Complete list of 16-bit UUIDs.
            data_dict[0x03] = uuids16
        if uuids128:
            # Complete list of 128-bit UUIDs
            data_dict[0x07] = uuids128

    if not _RE_IGNORABLE_NAME.fullmatch(device.name):
        # Complete name
        data_dict[0x09] = device.name.encode("utf-8")

    return data_dict
