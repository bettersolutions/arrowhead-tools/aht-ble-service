from typing import Optional
from uuid import UUID

from sqlalchemy import select, update

from gateway.db import database
from gateway.models import Node, node_table


async def save(node: Node) -> Node:
    query = node_table.insert().values(
        handle=node.handle,
        bdaddr=node.bdaddr,
        paired=node.paired
    )
    await database.execute(query)
    return node


async def find_by_address(address: str) -> Optional[Node]:
    query = select(Node).where(Node.bdaddr == address)
    return await database.fetch_one(query)


async def find_by_handle(handle: UUID) -> Optional[Node]:
    query = select(Node).where(Node.handle == handle)
    return await database.fetch_one(query)


async def find_all_paired():
    query = select(Node).where(Node.paired == True)
    return await database.fetch_all(query)


async def update_paired(address: str, paired: bool):
    query = update(Node).values(paired=paired).where(Node.bdaddr == address)
    await database.execute(query)
