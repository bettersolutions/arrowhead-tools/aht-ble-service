from typing import Any, Optional, Mapping
from uuid import UUID

from starlette.exceptions import HTTPException
from starlette.requests import HTTPConnection
from starlette.responses import JSONResponse, Response
from starlette.routing import Route

from gateway.gap import node_service


async def get_nodes(request: HTTPConnection) -> Response:
    passive = request.query_params.get("passive")
    timeout = request.query_params.get("timeout")
    enable = request.query_params.get("enable")
    if enable:
        nodes: Mapping[str, Any] = await node_service.get_paired_nodes(None, request.base_url)
        return JSONResponse(nodes)
    if timeout:
        try:
            timeout = float(timeout)
        except ValueError:
            raise HTTPException(400, "Timeout has to be float")
    mode = 'passive' if passive == '1' else 'active'
    nodes: Mapping[str, Any] = await node_service.get_nodes(mode, timeout, request.base_url)
    return JSONResponse(nodes)


async def manage_node(request: HTTPConnection) -> Response:
    name = request.query_params.get("name")
    if name is not None:
        return await get_node_name(request)
    return await manage_node_connection(request)


async def get_node_name(request: HTTPConnection) -> Response:
    handle: UUID = request.path_params.get("handle")
    return JSONResponse(await node_service.get_node_name(handle=handle, hostname=request.base_url))


async def manage_node_connection(request: HTTPConnection) -> Response:
    handle: UUID = request.path_params.get("handle")
    connect: Optional[str] = request.query_params.get("connect")
    if connect and connect not in ["0", "1"]:
        raise HTTPException(400, "Connect can be only 0 or 1")
    enable: Optional[str] = request.query_params.get("enable")
    if enable and enable not in ["0", "1"]:
        raise HTTPException(400, "Enable can be only 0 or 1")
    interval: Optional[float] = request.query_params.get("interval")
    if interval:
        try:
            interval = float(interval)
        except ValueError:
            raise HTTPException(400, "Interval has to be a float")
    rsp = await node_service.manage_connection(handle=handle, connect=connect, enable=enable, interval=interval,
                                               hostname=request.base_url)
    return JSONResponse(rsp) if rsp else Response(status_code=200)


routes = [
    Route("/nodes", endpoint=get_nodes, methods=["GET", ], name="nodes"),
    Route("/nodes/{handle:uuid}", endpoint=manage_node, methods=["GET", "PUT", ])
]
