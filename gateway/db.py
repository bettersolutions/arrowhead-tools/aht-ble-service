import databases

from gateway.settings import DATABASE_URL

database = databases.Database(DATABASE_URL)
