import os

import alembic.config

from db import db_dir

alembic_args = [
    '-c', os.path.join(db_dir, 'alembic.ini'),
    'upgrade', 'head'
]


def main():
    alembic.config.main(argv=alembic_args)
