"""Init table

Revision ID: f66762078218
Revises: 
Create Date: 2021-10-01 15:46:33.885659

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
from sqlalchemy_utils import UUIDType

revision = 'f66762078218'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'nodes',
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("handle", UUIDType(binary=False), unique=True),
        sa.Column("bdaddr", sa.String(255), unique=True),
        sa.Column("paired", sa.Boolean),
    )

    op.create_table(
        'attributes',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('handle', UUIDType(binary=False), nullable=True),
        sa.Column('uuid', UUIDType(binary=False)),
        sa.Column('node_id', sa.Integer(), nullable=True),
        sa.Column('notification_enabled', sa.Boolean(), nullable=True),
        sa.Column('notification_support', sa.Boolean(), nullable=True),
        sa.Column('value', sa.String(length=255), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['node_id'], ['nodes.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('handle'),
    )


def downgrade():
    op.drop_table('attributes')
    op.drop_table('nodes')
